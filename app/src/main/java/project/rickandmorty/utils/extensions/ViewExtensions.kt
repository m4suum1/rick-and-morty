package project.rickandmorty.utils.extensions

import android.content.Context
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import com.google.android.material.textfield.TextInputLayout
import project.rickandmorty.R
import project.rickandmorty.characterspage.model.CharactersPageParams

private const val PASSWORD_MAX_SIZE = 20
private const val PASSWORD_MIN_SIZE = 6
private const val EMAIL_MAX_SIZE = 24
private const val EMAIL_MIN_SIZE = 9

fun TextInputLayout.catchPasswordError(text: String) {
    if (text.length in PASSWORD_MIN_SIZE..PASSWORD_MAX_SIZE) {
        isErrorEnabled = false
    } else if (text.length < PASSWORD_MIN_SIZE) {
        error =
            resources.getString(R.string.text_input_layout_password_error_min)
        isErrorEnabled = true
    } else {
        error =
            resources.getString(R.string.text_input_layout_password_error_max)
        isErrorEnabled = true
    }
}

fun TextInputLayout.catchEmailError(text: String) {
    if (text.length in EMAIL_MIN_SIZE..EMAIL_MAX_SIZE) {
        isErrorEnabled = false
    } else if (text.length < EMAIL_MIN_SIZE) {
        error =
            resources.getString(R.string.text_input_layout_email_error_min)
        isErrorEnabled = true
    } else {
        error =
            resources.getString(R.string.text_input_layout_email_error_max)
        isErrorEnabled = true
    }
}

fun TextView.changeStatusColor(status: String, context: Context) {
    when (status) {
        "Alive" -> setTextColor(
            ContextCompat.getColor(
                context,
                R.color.green_forest
            )
        )
        "Dead" -> setTextColor(
            ContextCompat.getColor(
                context,
                R.color.red
            )
        )
        "unknown" -> setTextColor(
            ContextCompat.getColor(
                context,
                R.color.grey
            )
        )
    }
}

fun SearchView.doAfterTextChanged(
    getCharactersList: (
        params: CharactersPageParams
    ) -> Unit
) {
    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean {
            val charactersPageParams = CharactersPageParams(name = query)
            getCharactersList(charactersPageParams)
            return true
        }

        override fun onQueryTextChange(newText: String): Boolean {
            val charactersPageParams = CharactersPageParams(name = newText)
            getCharactersList(charactersPageParams)
            return true
        }
    })
}
