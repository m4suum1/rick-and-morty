package project.rickandmorty.utils.extensions

fun formatEpisodeNum(url: String) =
    buildString {
        append("Episode № ")
            .append(url.split("/").last())
    }
