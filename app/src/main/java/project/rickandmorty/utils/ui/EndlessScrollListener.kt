package project.rickandmorty.utils.ui

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import project.rickandmorty.characterspage.model.CharactersPageConstants.NULL_COUNT

private const val VISIBLE_THRESHOLD = 10
private const val DEFAULT_ITEMS_COUNT = 20
private const val FIRST_PAGE = 1

class EndlessScrollListener(
    private val layoutManager: LinearLayoutManager,
    private val loadNextPage: (totalItems: Int, page: Int) -> Unit
) : RecyclerView.OnScrollListener() {

    private var isLoading = true
    private var totalLoadedItems = NULL_COUNT

    private var currentPage = NULL_COUNT


    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        currentPage = layoutManager.itemCount/DEFAULT_ITEMS_COUNT
        val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

        if (totalItemCount > totalLoadedItems) {
            isLoading = false
            totalLoadedItems = totalItemCount
            return
        }

        val shouldLoadMore =
            totalItemCount - visibleItemCount <= firstVisibleItem + VISIBLE_THRESHOLD && firstVisibleItem != NULL_COUNT

        if (!isLoading && shouldLoadMore) {
            currentPage++
            loadNextPage(totalItemCount, currentPage)
            isLoading = true
        }
    }

    fun reset() {
        isLoading = false
        totalLoadedItems = NULL_COUNT
        currentPage = FIRST_PAGE
    }
}
