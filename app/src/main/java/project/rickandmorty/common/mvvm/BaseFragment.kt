package project.rickandmorty.common.mvvm

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import timber.log.Timber

abstract class BaseFragment(@LayoutRes layoutRes: Int): Fragment(layoutRes) {
    abstract fun bind()
    abstract fun initViews(view: View)

    private val coroutineScope = CoroutineScope(Job() + Dispatchers.Main)

    protected fun launch(block: suspend CoroutineScope.() -> Unit) {
        coroutineScope.launch(block = block)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(view)
        bind()
        Timber.d("/=/ onViewCreated ${this.javaClass.name}")
    }

    override fun onStart() {
        super.onStart()
        Timber.d("/=/ onStart ${this.javaClass.name}")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        coroutineScope.cancel()
        Timber.d("/=/ onDestroyView ${this.javaClass.name}")
    }

    override fun onStop() {
        super.onStop()
        Timber.d("/=/ onStop ${this.javaClass.name}")
    }

    override fun onPause() {
        super.onPause()
        Timber.d("/=/ onPause ${this.javaClass.name}")
    }

    override fun onDetach() {
        super.onDetach()
        Timber.d("/=/ onDetach ${this.javaClass.name}")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Timber.d("/=/ onAttach ${this.javaClass.name}")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("/=/ onCreate ${this.javaClass.name}")
    }

    fun <T : Any, F : Flow<T>> observe(flow: F, body: (T) -> Unit) {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                flow.collect { body(it) }
            }
        }
    }

    fun <T : Any?, F : Flow<T?>> observeNullable(flow: F, body: (T?) -> Unit) {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                flow.collect { body(it) }
            }
        }
    }
}
