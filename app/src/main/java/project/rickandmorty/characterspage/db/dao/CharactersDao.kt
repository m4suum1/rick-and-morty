package project.rickandmorty.characterspage.db.dao


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import project.rickandmorty.characterspage.db.model.CharacterEntity
import project.rickandmorty.characterspage.db.model.EpisodeEntity

@Dao
interface CharactersDao {
    @Query("SELECT EPISODE_URL FROM episodes WHERE CHARACTER_ID = :characterId")
    fun getEpisodes(characterId: Int): Flow<List<String>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addEpisodes(episodes: List<EpisodeEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addCharacters(characters: List<CharacterEntity>)

    @Query("SELECT * FROM characters")
    fun getCharacters(): Flow<List<CharacterEntity>>

    @Query("SELECT * FROM characters ORDER BY ID DESC LIMIT 20")
    fun getLastCharacters(): Flow<List<CharacterEntity>>
}
