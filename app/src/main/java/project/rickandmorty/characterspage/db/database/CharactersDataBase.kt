package project.rickandmorty.characterspage.db.database

import androidx.room.Database
import androidx.room.RoomDatabase
import project.rickandmorty.characterspage.db.dao.CharactersDao
import project.rickandmorty.characterspage.db.model.CharacterEntity
import project.rickandmorty.characterspage.db.model.EpisodeEntity

@Database(
    entities = [CharacterEntity::class, EpisodeEntity::class], version = 1, exportSchema = false
)
abstract class CharactersDataBase : RoomDatabase() {
    abstract val charactersDao: CharactersDao
}
