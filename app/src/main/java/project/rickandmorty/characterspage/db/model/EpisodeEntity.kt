package project.rickandmorty.characterspage.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
@Entity(
    tableName = "episodes",
    indices = [
        Index("CHARACTER_ID")
    ]
)
data class EpisodeEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    val id: Int,
    @ColumnInfo(name = "CHARACTER_ID")
    val characterId: Int,
    @ColumnInfo(name = "EPISODE_URL")
    val episodeUrl: String
)
