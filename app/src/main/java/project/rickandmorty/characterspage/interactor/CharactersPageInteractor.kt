package project.rickandmorty.characterspage.interactor

import kotlinx.coroutines.*
import project.rickandmorty.characterspage.di.CharactersPageModule.Companion.DefaultDispatcher
import project.rickandmorty.characterspage.model.CharactersList
import project.rickandmorty.characterspage.model.CharactersPageConstants.BASE_DATA_CHARACTER_FULL_COUNT
import project.rickandmorty.characterspage.model.CharactersPageConstants.NULL_COUNT
import project.rickandmorty.characterspage.model.CharactersPageParams
import project.rickandmorty.characterspage.model.Info
import project.rickandmorty.characterspage.repository.CharactersPageLocalRepository
import project.rickandmorty.characterspage.repository.CharactersPageRemoteRepository
import project.rickandmorty.characterspage.ui.model.CharactersListUi
import project.rickandmorty.characterspage.ui.model.CharactersPageConverterUi
import javax.inject.Inject

class CharactersPageInteractor @Inject constructor(
    private val remoteRepository: CharactersPageRemoteRepository,
    private val localRepository: CharactersPageLocalRepository,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) {

    @OptIn(ExperimentalCoroutinesApi::class)
    suspend fun getCharactersList(scope: CoroutineScope): CharactersListUi {
        return suspendCancellableCoroutine { continuation ->
            val job = scope.launch {
                localRepository.getCharactersList(scope).collect { list ->
                    withContext(defaultDispatcher) {
                        val characterData =
                            CharactersPageConverterUi.convert(
                                CharactersList(
                                    Info(
                                        BASE_DATA_CHARACTER_FULL_COUNT,
                                        NULL_COUNT,
                                        null,
                                        null
                                    ), list
                                )
                            )
                        if (continuation.isActive) {
                            continuation.resume(characterData) {
                                this.cancel()
                            }
                        }
                    }
                    return@collect
                }
            }
            continuation.invokeOnCancellation { job.cancel() }
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    suspend fun getLastCharacters(scope: CoroutineScope): CharactersListUi {
        return suspendCancellableCoroutine { continuation ->
            val job = scope.launch {
                localRepository.getLastCharacters(scope).collect { list ->
                    withContext(defaultDispatcher) {
                        val characterData =
                            CharactersPageConverterUi.convert(
                                CharactersList(
                                    Info(BASE_DATA_CHARACTER_FULL_COUNT, NULL_COUNT, null, null),
                                    list.sortedBy {
                                        it.id
                                    })
                            )
                        if (continuation.isActive) {
                            continuation.resume(characterData) {
                                this.cancel()
                            }
                        }
                    }
                    return@collect
                }
            }
            continuation.invokeOnCancellation { job.cancel() }
        }
    }

    suspend fun putCharactersList(params: CharactersPageParams) {
        val data = remoteRepository.getCharactersList(params)
        localRepository.putCharacterList(data)
    }
}
