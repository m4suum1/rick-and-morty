package project.rickandmorty.characterspage.repository

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import project.rickandmorty.characterspage.model.Character
import project.rickandmorty.characterspage.model.CharactersList

interface CharactersPageLocalRepository {
    suspend fun getCharactersList(scope: CoroutineScope): Flow<List<Character>>
    suspend fun putCharacterList(data: CharactersList)
    suspend fun getLastCharacters(scope: CoroutineScope): Flow<List<Character>>
}
