package project.rickandmorty.characterspage.repository

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import project.rickandmorty.characterspage.db.dao.CharactersDao
import project.rickandmorty.characterspage.db.model.CharacterEntity
import project.rickandmorty.characterspage.db.model.EpisodeEntity
import project.rickandmorty.characterspage.di.CharactersPageModule.Companion.DefaultDispatcher
import project.rickandmorty.characterspage.model.Character
import project.rickandmorty.characterspage.model.CharactersList
import project.rickandmorty.characterspage.model.CharactersPageConstants.NULL_COUNT
import project.rickandmorty.characterspage.model.Location
import project.rickandmorty.characterspage.model.Origin
import javax.inject.Inject

class CharactersPageLocalRepositoryImpl @Inject constructor(
    private val dao: CharactersDao,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) : CharactersPageLocalRepository {

    override suspend fun putCharacterList(data: CharactersList) {
        withContext(defaultDispatcher) {
            val charactersEntity = data.results.map {
                CharacterEntity(
                    count = data.info.count,
                    id = it.id,
                    name = it.name,
                    status = it.status,
                    species = it.species,
                    type = it.type,
                    gender = it.gender,
                    originName = it.origin.name,
                    originUrl = it.origin.url,
                    locationName = it.location.name,
                    locationUrl = it.location.url,
                    image = it.image,
                    created = it.created
                )
            }
            dao.addCharacters(characters = charactersEntity)
            val episodesEntity: MutableList<EpisodeEntity> = mutableListOf()
            data.results.forEach { character ->
                episodesEntity += character.episode.map {
                    EpisodeEntity(
                        id = NULL_COUNT,
                        characterId = character.id,
                        episodeUrl = it
                    )
                }
            }
            dao.addEpisodes(episodes = episodesEntity)
        }
    }

    override suspend fun getCharactersList(scope: CoroutineScope): Flow<List<Character>> {
        return dao.getCharacters().flowOn(defaultDispatcher).map { list ->
            list.map {
                Character(
                    id = it.id,
                    name = it.name,
                    status = it.status,
                    species = it.species,
                    type = it.type,
                    gender = it.gender,
                    origin = Origin(it.originName, it.originUrl),
                    location = Location(it.locationName, it.locationUrl),
                    image = it.image,
                    episode = findEpisodes(it.id, scope),
                    created = it.created
                )
            }
        }
    }

    override suspend fun getLastCharacters(scope: CoroutineScope): Flow<List<Character>> {
        return dao.getLastCharacters().flowOn(defaultDispatcher).map { list ->
            list.map {
                Character(
                    id = it.id,
                    name = it.name,
                    status = it.status,
                    species = it.species,
                    type = it.type,
                    gender = it.gender,
                    origin = Origin(it.originName, it.originUrl),
                    location = Location(it.locationName, it.locationUrl),
                    image = it.image,
                    episode = findEpisodes(it.id, scope),
                    created = it.created
                )
            }
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private suspend fun findEpisodes(id: Int, scope: CoroutineScope): List<String> {
        return suspendCancellableCoroutine { continuation ->
            val job = scope.launch {
                dao.getEpisodes(id).collect { episodes ->
                    withContext(defaultDispatcher) {
                        if (continuation.isActive) {
                            continuation.resume(episodes.distinctBy { it }) {
                                this.cancel()
                            }
                        }
                    }
                    return@collect
                }
            }
            continuation.invokeOnCancellation { job.cancel() }
        }
    }
}
