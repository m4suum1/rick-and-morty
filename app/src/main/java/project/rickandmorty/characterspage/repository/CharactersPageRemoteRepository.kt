package project.rickandmorty.characterspage.repository

import project.rickandmorty.characterspage.model.CharactersList
import project.rickandmorty.characterspage.model.CharactersPageParams

interface CharactersPageRemoteRepository {
    suspend fun getCharactersList(params: CharactersPageParams): CharactersList
}
