package project.rickandmorty.characterspage.repository

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import project.rickandmorty.characterspage.api.CharactersPageApi
import project.rickandmorty.characterspage.di.CharactersPageModule.Companion.IoDispatcher
import project.rickandmorty.characterspage.model.CharactersPageConverter
import project.rickandmorty.characterspage.model.CharactersPageParams
import javax.inject.Inject

class CharactersPageRemoteRepositoryImpl @Inject constructor(
    private val api: CharactersPageApi,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : CharactersPageRemoteRepository {
    override suspend fun getCharactersList(params: CharactersPageParams) =
        withContext(ioDispatcher) {
            CharactersPageConverter.convert(
                api.getCharactersList(
                    page = params.page,
                    name = params.name,
                    status = params.status,
                    species = params.species,
                    type = params.type,
                    gender = params.gender
                )
            )
        }
}
