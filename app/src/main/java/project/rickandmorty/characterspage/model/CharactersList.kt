package project.rickandmorty.characterspage.model

data class CharactersList(
    val info: Info,
    val results: List<Character>
)
