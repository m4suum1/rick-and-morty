package project.rickandmorty.characterspage.model

data class Location(
    val name: String,
    val url: String
)
