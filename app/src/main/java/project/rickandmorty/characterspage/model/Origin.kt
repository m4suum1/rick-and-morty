package project.rickandmorty.characterspage.model

data class Origin(
    val name: String,
    val url: String
)
