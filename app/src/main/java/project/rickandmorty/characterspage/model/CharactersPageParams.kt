package project.rickandmorty.characterspage.model

data class CharactersPageParams(
    val page: Int = 0,
    val name: String = "",
    val status: String = "",
    val species: String = "",
    val type: String = "",
    val gender: String = ""
)