package project.rickandmorty.characterspage.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import project.rickandmorty.characterspage.ui.model.CharacterUi
import project.rickandmorty.databinding.ItemCharacterBinding
import project.rickandmorty.utils.extensions.changeStatusColor

class CharactersPageViewHolder(
    private val binding: ItemCharacterBinding,
    private val clickOnItem: (CharacterUi) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    constructor(
        parent: ViewGroup,
        onClickItem: (CharacterUi) -> Unit
    ) : this(
        ItemCharacterBinding.inflate(LayoutInflater.from(parent.context), parent, false),
        onClickItem
    )

    private lateinit var characterName: String
    private lateinit var characterStatus: String
    private lateinit var characterSpecies: String
    private lateinit var characterIcon: String
    private lateinit var item: CharacterUi

    fun getData(item: CharacterUi) {
        characterName = item.name
        characterStatus = item.status
        characterSpecies = item.species
        characterIcon = item.image
        this.item = item
    }

    fun onBind() {
        with(binding) {
            textViewName.text = characterName
            textViewStatusValue.text = characterStatus
            textViewStatusValue.changeStatusColor(characterStatus, itemView.context)
            textViewSpeciesValue.text = characterSpecies
            imageViewIcon.load(characterIcon) {
                crossfade(true)
                crossfade(1000)
                transformations(CircleCropTransformation())
            }
        }
        itemView.setOnClickListener {
            clickOnItem(item)
        }
    }
}
