package project.rickandmorty.characterspage.ui.model

import project.rickandmorty.characterspage.model.CharactersList

object CharactersPageConverterUi {

    fun convert(data: CharactersList) =
        CharactersListUi(
            count = data.info.count,
            results = convert(data.results)
        )

    private fun convert(characters: List<project.rickandmorty.characterspage.model.Character>) =
        characters.map {
            CharacterUi(
                id = it.id,
                name = it.name,
                status = it.status,
                species = it.species,
                type = convertEmptyString(it.type),
                gender = it.gender,
                origin = it.origin.name,
                location = it.location.name,
                image = it.image,
                episode = it.episode
            )
        }

    private fun convertEmptyString(string: String): String {
        return when (string) {
            "" -> return "-"
            else -> string
        }
    }
}
