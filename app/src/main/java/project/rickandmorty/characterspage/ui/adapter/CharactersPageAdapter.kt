package project.rickandmorty.characterspage.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import project.rickandmorty.R
import project.rickandmorty.characterspage.ui.model.CharacterUi
import project.rickandmorty.utils.ui.PagingState
import java.util.*


private const val ONE = 1

class CharactersPageAdapter(
    private val clickOnItem: (CharacterUi) -> Unit
) : ListAdapter<CharacterUi, CharactersPageViewHolder>(DIFF_CALLBACK) {

    private var pagingState: PagingState = PagingState.Idle
    private val noAdditionalItemRequiredState = listOf(PagingState.Idle)

    private val data = mutableListOf<CharacterUi>()
    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<CharacterUi> = object : DiffUtil.ItemCallback<CharacterUi>() {
            override fun areItemsTheSame(oldItem: CharacterUi, newItem: CharacterUi): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: CharacterUi, newItem: CharacterUi): Boolean {
                return oldItem.name == newItem.name && oldItem.image == newItem.image
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharactersPageViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.item_character, parent, false)
        return CharactersPageViewHolder(parent, clickOnItem)
    }

    override fun getItemViewType(position: Int): Int = when {
        !stateRequiresExtraItem(pagingState) || position < itemCount - ONE -> {
            R.layout.item_character
        }
        pagingState is PagingState.Loading || pagingState is PagingState.InitialLoading -> {
            R.layout.item_progressbar
        }
        else -> R.layout.item_error
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: CharactersPageViewHolder, position: Int) {
        val listItem = getItem(position)
        holder.getData(listItem)
        holder.onBind()
    }

    fun setPagingState(newState: PagingState) {
        if (pagingState::class.java == newState::class.java) return
        val shouldHasExtraItem = stateRequiresExtraItem(newState)
        val hasExtraItem = stateRequiresExtraItem(pagingState)

        pagingState = newState

        // since item count is a function - cache its value.
        val count = itemCount
        when {
            hasExtraItem && shouldHasExtraItem -> {
                notifyItemChanged(count - ONE)
            }
            hasExtraItem && !shouldHasExtraItem -> {
                notifyItemRemoved(count - ONE)
            }
            !hasExtraItem && shouldHasExtraItem -> {
                notifyItemInserted(count)
            }
        }
    }

    private fun stateRequiresExtraItem(state: PagingState) = state !in noAdditionalItemRequiredState

    fun addData(items: List<CharacterUi>) {
        data.addAll(items)
        submitList(data)
    }

    @SuppressLint("NotifyDataSetChanged")
    //for searching
    fun setData(items: List<CharacterUi>) {
        data.clear()
        data.addAll(items)
        notifyDataSetChanged()
    }
}
