package project.rickandmorty.characterspage.ui

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import project.rickandmorty.characterspage.di.CharactersPageModule.Companion.DefaultDispatcher
import project.rickandmorty.characterspage.interactor.CharactersPageInteractor
import project.rickandmorty.characterspage.model.CharactersPageConstants.NULL_COUNT
import project.rickandmorty.characterspage.model.CharactersPageParams
import project.rickandmorty.characterspage.ui.model.CharactersListUi
import project.rickandmorty.common.mvvm.BaseViewModel
import project.rickandmorty.utils.ui.PagingState
import timber.log.Timber
import javax.inject.Inject


@HiltViewModel
class CharactersPageViewModel @Inject constructor(
    private val interactor: CharactersPageInteractor,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) : BaseViewModel() {

    private var pagingJob: Job? = null

    private val _charactersListStateFlow =
        MutableStateFlow(CharactersListUi(NULL_COUNT, emptyList()))
    val charactersListStateFlow = _charactersListStateFlow.asStateFlow()

    private val _loadingStateFlow = MutableStateFlow(false)
    val loadingStateFlow = _loadingStateFlow.asStateFlow()

    private val _pagingState: MutableStateFlow<PagingState> = MutableStateFlow(PagingState.Idle)
    val pagingState = _pagingState.asStateFlow()

    private val _page = MutableStateFlow(NULL_COUNT)
    val page = _page.asStateFlow()

    fun getSavedData() {
        launch {
            val data = interactor.getCharactersList(this)
            _charactersListStateFlow.tryEmit(data)
        }
    }

    fun getLastSavedData(){
        launch {
            val data = interactor.getLastCharacters(this)
            _charactersListStateFlow.tryEmit(data)
        }
    }

    fun loadCharactersList(params: CharactersPageParams) {
        _loadingStateFlow.tryEmit(true)
        _pagingState.tryEmit(PagingState.InitialLoading)
        pagingJob?.cancel()
        pagingJob = launch {
            try {
                withContext(defaultDispatcher) {
                    interactor.putCharactersList(params)
                }
                _pagingState.tryEmit(PagingState.Idle)
            } catch (e: CancellationException) {
                Timber.e("/*/ Error ${e.message}")
            } catch (t: Throwable) {
                Timber.e("/*/ Error ${t.message}")
            } finally {
                _loadingStateFlow.tryEmit(false)
            }
        }
    }

    fun setCurrentPage(page: Int) {
        _page.tryEmit(page)
    }
}
