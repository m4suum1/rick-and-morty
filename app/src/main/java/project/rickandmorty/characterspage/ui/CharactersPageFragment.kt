package project.rickandmorty.characterspage.ui

import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import project.rickandmorty.R
import project.rickandmorty.characterdetailpage.ui.CharacterDetailPageFragment
import project.rickandmorty.characterspage.model.CharactersPageConstants.BASE_DATA_CHARACTER_FULL_COUNT
import project.rickandmorty.characterspage.model.CharactersPageConstants.NULL_COUNT
import project.rickandmorty.characterspage.model.CharactersPageParams
import project.rickandmorty.characterspage.ui.adapter.CharactersPageAdapter
import project.rickandmorty.characterspage.ui.model.CharactersListUi
import project.rickandmorty.common.mvvm.BaseFragment
import project.rickandmorty.databinding.FragmentCharactersPageBinding
import project.rickandmorty.utils.extensions.doAfterTextChanged
import project.rickandmorty.utils.extensions.popScreen
import project.rickandmorty.utils.extensions.replaceScreen
import project.rickandmorty.utils.ui.EndlessScrollListener
import project.rickandmorty.utils.viewbinding.viewBinding


@AndroidEntryPoint
class CharactersPageFragment : BaseFragment(R.layout.fragment_characters_page) {
    private val binding: FragmentCharactersPageBinding by viewBinding()
    private val viewModel: CharactersPageViewModel by viewModels()

    private val adapter: CharactersPageAdapter by lazy {
        CharactersPageAdapter { item ->
            replaceScreen(CharacterDetailPageFragment.newInstance(item))
        }
    }

    private val searchItem: MenuItem by lazy {
        with(binding.toolbar) {
            inflateMenu(R.menu.characters_menu)
            menu.findItem(R.id.action_search)
        }
    }

    private val searchView: SearchView by lazy {
        searchItem.actionView as SearchView
    }

    private var charactersData: CharactersListUi? = null

    private lateinit var layoutManager: LinearLayoutManager

    private lateinit var scrollListener: EndlessScrollListener

    override fun bind() {
        with(viewModel) {
            observe(page) { page ->
                when {
                    page != NULL_COUNT -> {
                        loadCharactersList(CharactersPageParams(page))
                        getLastSavedData()

                    }
                }
            }

            observe(charactersListStateFlow) { charactersListStateFlow ->
                when {
                    charactersListStateFlow == CharactersListUi(
                        BASE_DATA_CHARACTER_FULL_COUNT,
                        emptyList()
                    ) -> {
                        loadCharactersList(CharactersPageParams())
                    }
                    charactersListStateFlow != charactersData -> {
                        charactersData = charactersListStateFlow
                        when (charactersData!!.count) {
                            BASE_DATA_CHARACTER_FULL_COUNT -> {
                                adapter.addData(charactersData!!.results)
                            }
                        }
                    }
                }
            }

            observe(loadingStateFlow) { isLoading ->
                binding.progressBar.isVisible = isLoading
            }

            observe(pagingState) { pagingState ->
                adapter.setPagingState(pagingState)
            }
        }
    }

    override fun initViews(view: View) {
        if (charactersData == null)
            viewModel.getSavedData()
        with(binding) {
            progressBar.isVisible = true
            requireActivity().window.statusBarColor =
                ContextCompat.getColor(requireContext(), R.color.oily_yellow)

            layoutManager = LinearLayoutManager(requireContext())
            scrollListener = EndlessScrollListener(layoutManager) { _, page ->
                viewModel.setCurrentPage(page)
            }
            recyclerViewCharacters.layoutManager = layoutManager
            recyclerViewCharacters.setHasFixedSize(true)
            recyclerViewCharacters.adapter = adapter
            adapter.onAttachedToRecyclerView(recyclerViewCharacters)
            recyclerViewCharacters.addOnScrollListener(scrollListener)

            toolbar.setNavigationOnClickListener {
                popScreen()
            }

            searchView.doAfterTextChanged {
                viewModel.loadCharactersList(it)
            }

            searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
                override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                    return true
                }

                override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                    scrollListener.reset()
                    viewModel.loadCharactersList(CharactersPageParams())
                    return true
                }
            })
        }
    }
}
