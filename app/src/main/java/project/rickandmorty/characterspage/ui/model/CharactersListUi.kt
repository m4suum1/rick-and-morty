package project.rickandmorty.characterspage.ui.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CharactersListUi(
    val count: Int,
    val results: List<CharacterUi>
):Parcelable
