package project.rickandmorty.characterspage.api.model

import com.google.gson.annotations.SerializedName

data class CharactersListResponse(
    @SerializedName("info")
    val info: InfoResponse,
    @SerializedName("results")
    val results: List<CharacterResponse>
)
