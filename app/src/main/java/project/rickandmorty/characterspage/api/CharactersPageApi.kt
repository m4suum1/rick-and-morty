package project.rickandmorty.characterspage.api

import project.rickandmorty.characterspage.api.model.CharactersListResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CharactersPageApi {
    @GET("character")
    suspend fun getCharactersList(
        @Query("page") page: Int = 0,
        @Query("name") name: String = "",
        @Query("status") status: String = "",
        @Query("species") species: String = "",
        @Query("type") type: String = "",
        @Query("gender") gender: String = "",
    ): CharactersListResponse
}
