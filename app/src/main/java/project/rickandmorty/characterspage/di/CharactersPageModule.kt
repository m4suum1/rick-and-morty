package project.rickandmorty.characterspage.di

import android.content.Context
import androidx.room.Room
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import project.rickandmorty.characterspage.api.CharactersPageApi
import project.rickandmorty.characterspage.db.dao.CharactersDao
import project.rickandmorty.characterspage.db.database.CharactersDataBase
import project.rickandmorty.characterspage.interactor.CharactersPageInteractor
import project.rickandmorty.characterspage.repository.CharactersPageLocalRepository
import project.rickandmorty.characterspage.repository.CharactersPageLocalRepositoryImpl
import project.rickandmorty.characterspage.repository.CharactersPageRemoteRepository
import project.rickandmorty.characterspage.repository.CharactersPageRemoteRepositoryImpl
import retrofit2.Retrofit
import javax.inject.Qualifier
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
abstract class CharactersPageModule {

    @Binds
    @Singleton
    abstract fun bindRemoteRepository(repository: CharactersPageRemoteRepositoryImpl): CharactersPageRemoteRepository

    @Binds
    @Singleton
    abstract fun bindLocalRepository(repository: CharactersPageLocalRepositoryImpl): CharactersPageLocalRepository

    companion object {

        @IoDispatcher
        @Provides
        fun provideContextIo(): CoroutineDispatcher = Dispatchers.IO

        @DefaultDispatcher
        @Provides
        fun provideContextDefault(): CoroutineDispatcher = Dispatchers.Default

        @Singleton
        @Provides
        fun providesCoroutineScope(
            @IoDispatcher ioDispatcher: CoroutineDispatcher
        ): CoroutineScope = CoroutineScope(SupervisorJob() + ioDispatcher)



        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class IoDispatcher

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class DefaultDispatcher

        @Provides
        @Singleton
        fun provideApi(retrofit: Retrofit): CharactersPageApi =
            retrofit.create(CharactersPageApi::class.java)

        @Provides
        @Singleton
        fun provideInteractor(
            remoteRepository: CharactersPageRemoteRepository,
            localRepository: CharactersPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): CharactersPageInteractor =
            CharactersPageInteractor(remoteRepository, localRepository, defaultDispatcher)

        @Provides
        @Singleton
        fun provideChannelDao(appDatabase: CharactersDataBase): CharactersDao =
            appDatabase.charactersDao

        @Provides
        @Singleton
        fun provideAppDatabase(@ApplicationContext appContext: Context): CharactersDataBase =
            Room.databaseBuilder(
                appContext,
                CharactersDataBase::class.java,
                "Database"
            ).build()
    }
}
