package project.rickandmorty.characterdetailpage.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import project.rickandmorty.R

class CharacterDetailPageAdapter (
    private val clickOnItem:(String) -> Unit
) : RecyclerView.Adapter<CharacterDetailPageViewHolder>() {

    private val data = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterDetailPageViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.item_character, parent, false)
        return CharacterDetailPageViewHolder(parent, clickOnItem)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: CharacterDetailPageViewHolder, position: Int) {
        val listItem = data[position]
        holder.getData(listItem)
        holder.onBind()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(items: List<String>) {
        data.clear()
        data.addAll(items)
        notifyDataSetChanged()
    }
}
