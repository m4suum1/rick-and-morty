package project.rickandmorty.characterdetailpage.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import project.rickandmorty.databinding.ItemDetailCharacterEpisodesBinding
import project.rickandmorty.utils.extensions.formatEpisodeNum

class CharacterDetailPageViewHolder(
    private val binding: ItemDetailCharacterEpisodesBinding,
    private val clickOnItem: (String) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    constructor(
        parent: ViewGroup,
        onClickItem: (String) -> Unit
    ) : this(
        ItemDetailCharacterEpisodesBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ),
        onClickItem
    )

    private lateinit var item: String

    fun getData(item: String) {
        this.item = item
    }

    fun onBind() {
        with(binding) {
            textViewEpisodeName.text = formatEpisodeNum(item)
        }
        itemView.setOnClickListener {
            clickOnItem(item)
        }
    }
}
