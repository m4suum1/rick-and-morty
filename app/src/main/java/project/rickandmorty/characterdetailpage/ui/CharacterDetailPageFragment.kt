package project.rickandmorty.characterdetailpage.ui

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import coil.transform.CircleCropTransformation
import dagger.hilt.android.AndroidEntryPoint
import project.rickandmorty.R
import project.rickandmorty.characterdetailpage.ui.adapter.CharacterDetailPageAdapter
import project.rickandmorty.characterspage.ui.model.CharacterUi
import project.rickandmorty.common.mvvm.BaseFragment
import project.rickandmorty.databinding.FragmentCharacterDetailPageBinding
import project.rickandmorty.utils.Arguments
import project.rickandmorty.utils.extensions.changeStatusColor
import project.rickandmorty.utils.extensions.args
import project.rickandmorty.utils.extensions.withArgs
import project.rickandmorty.utils.extensions.popScreen
import project.rickandmorty.utils.viewbinding.viewBinding


@AndroidEntryPoint
class CharacterDetailPageFragment : BaseFragment(R.layout.fragment_character_detail_page) {
    private val binding: FragmentCharacterDetailPageBinding by viewBinding()
    private val adapter: CharacterDetailPageAdapter by lazy {
        CharacterDetailPageAdapter { item ->
            print(item)
        }
    }

    companion object {
        fun newInstance(character: CharacterUi) =
            CharacterDetailPageFragment().withArgs(Arguments.CHARACTER_DATA to character)
    }

    private val character: CharacterUi by args(Arguments.CHARACTER_DATA)
    override fun bind() {
        requireActivity().window.statusBarColor =
            ContextCompat.getColor(requireContext(), R.color.layout_background)
        with(binding) {
            imageViewIcon.load(character.image) {
                transformations(CircleCropTransformation())
            }
            textViewName.text = character.name
            textViewStatusValue.text = character.status
            textViewStatusValue.changeStatusColor(character.status, requireContext())
            textViewSpeciesValue.text = character.species
            textViewTypeValue.text = character.type
            textViewGenderValue.text = character.gender
            textViewOriginValue.text = character.origin
            textViewLocationValue.text = character.location
            adapter.setData(character.episode)

            imageViewBack.setOnClickListener {
                popScreen()
            }
        }
    }

    override fun initViews(view: View) {
        with(binding) {
            recyclerViewCharacters.layoutManager = LinearLayoutManager(requireContext())
            recyclerViewCharacters.setHasFixedSize(true)
            recyclerViewCharacters.adapter = adapter
            adapter.onAttachedToRecyclerView(recyclerViewCharacters)
        }
    }
}
