package project.rickandmorty.signinpage.ui

import android.view.View
import androidx.core.widget.doAfterTextChanged
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import project.rickandmorty.R
import project.rickandmorty.characterspage.di.CharactersPageModule.Companion.IoDispatcher
import project.rickandmorty.common.mvvm.BaseFragment
import project.rickandmorty.databinding.FragmentSignInPageBinding
import project.rickandmorty.menupage.ui.MenuPageFragment
import project.rickandmorty.registerpage.ui.RegisterPageFragment
import project.rickandmorty.utils.FirebaseException
import project.rickandmorty.utils.extensions.*
import project.rickandmorty.utils.viewbinding.viewBinding

@AndroidEntryPoint
class SignInPageFragment(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : BaseFragment(R.layout.fragment_sign_in_page) {

    private val binding: FragmentSignInPageBinding by viewBinding()
    private val auth: FirebaseAuth by lazy {
        Firebase.auth
    }

    override fun onStart() {
        super.onStart()
        if (auth.currentUser != null) {
            replaceScreen(MenuPageFragment(ioDispatcher), popCurrent = true)
        }
    }

    override fun bind() {
        with(binding) {
            textInputEditTextPassword.doAfterTextChanged {
                textInputLayoutPassword.catchPasswordError(it.toString())
            }
            textInputEditTextLogin.doAfterTextChanged {
                textInputLayoutLogin.catchEmailError(it.toString())
            }
            buttonSignIn.setOnClickListener {
                hideKeyboardDrop()
                val email = textInputEditTextLogin.text.toString()
                val password = textInputEditTextPassword.text.toString()
                if (email.isEmpty() || password.isEmpty()) createSnackBar(
                    myCoordinatorLayout,
                    resources.getString(R.string.all_fields_snack)
                )
                else if (email.isNotEmpty() && password.isNotEmpty()) {
                    launch {
                        signIn(auth, email, password, myCoordinatorLayout)
                    }
                }
            }
            textViewRegister.setOnClickListener {
                replaceScreen(RegisterPageFragment(ioDispatcher))
            }
        }
    }

    override fun initViews(view: View) {
    }

    private suspend fun signIn(
        auth: FirebaseAuth,
        email: String,
        password: String,
        myCoordinatorLayout: View
    ) {
        var authorization: Task<AuthResult>
        withContext(ioDispatcher) {
            authorization = auth.signInWithEmailAndPassword(email, password)
        }
        authorization.addOnCompleteListener { task ->
            if (task.isSuccessful) replaceScreen(
                MenuPageFragment(ioDispatcher),
                popCurrent = true
            )
            else createSnackBar(
                myCoordinatorLayout,
                FirebaseException.getErrorCode(task)
            )
        }
    }
}
