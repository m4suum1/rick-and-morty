package project.rickandmorty.registerpage.ui

import android.view.View
import androidx.core.widget.doAfterTextChanged
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import project.rickandmorty.R
import project.rickandmorty.characterspage.di.CharactersPageModule.Companion.IoDispatcher
import project.rickandmorty.common.mvvm.BaseFragment
import project.rickandmorty.databinding.FragmentRegisterPageBinding
import project.rickandmorty.menupage.ui.MenuPageFragment
import project.rickandmorty.utils.FirebaseException
import project.rickandmorty.utils.extensions.*
import project.rickandmorty.utils.viewbinding.viewBinding

@AndroidEntryPoint
class RegisterPageFragment(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : BaseFragment(R.layout.fragment_register_page) {
    private val binding: FragmentRegisterPageBinding by viewBinding()
    private val auth: FirebaseAuth by lazy {
        Firebase.auth
    }

    override fun bind() {
        with(binding) {
            textInputEditTextPassword.doAfterTextChanged {
                textInputLayoutPassword.catchPasswordError(it.toString())
            }
            textInputEditTextConfirmPassword.doAfterTextChanged {
                textInputLayoutConfirmPassword.catchPasswordError(it.toString())
            }
            textInputEditTextLogin.doAfterTextChanged {
                textInputLayoutLogin.catchEmailError(it.toString())
            }
            buttonRegister.setOnClickListener {
                hideKeyboardDrop()
                val email = textInputEditTextLogin.text.toString()
                val password = textInputEditTextPassword.text.toString()
                val confirmPassword = textInputEditTextConfirmPassword.text.toString()
                if (
                    email.isEmpty() ||
                    password.isEmpty() ||
                    confirmPassword.isEmpty()
                ) createSnackBar(
                    myCoordinatorLayout,
                    resources.getString(R.string.all_fields_snack)
                )
                else if (email.isNotEmpty() && password.isNotEmpty()) {
                    if (password == confirmPassword) {
                        launch {
                            signUp(auth, email, password, myCoordinatorLayout)
                        }
                    } else createSnackBar(
                        myCoordinatorLayout,
                        resources.getString(R.string.confirm_passwords_snack)
                    )
                }
            }
            buttonSignIn.setOnClickListener {
                popScreen()
            }
        }
    }

    override fun initViews(view: View) {
    }

    private suspend fun signUp(
        auth: FirebaseAuth,
        email: String,
        password: String,
        myCoordinatorLayout: View
    ) {
        var authorization: Task<AuthResult>
        withContext(ioDispatcher) {
            authorization = auth.createUserWithEmailAndPassword(email, password)
        }
        authorization.addOnCompleteListener { task ->
            if (task.isSuccessful) replaceScreen(
                MenuPageFragment(ioDispatcher),
                clearBackStack = true
            )
            else createSnackBar(
                myCoordinatorLayout,
                FirebaseException.getErrorCode(task)
            )
        }
    }
}
