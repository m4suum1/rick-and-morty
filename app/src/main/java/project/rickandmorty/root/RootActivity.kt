package project.rickandmorty.root

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import project.rickandmorty.R
import project.rickandmorty.databinding.ActivityRootBinding
import project.rickandmorty.signinpage.ui.SignInPageFragment
import project.rickandmorty.utils.extensions.popFeature

@AndroidEntryPoint
class RootActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRootBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRootBinding.inflate(layoutInflater)
        setContentView(binding.root)
        replace(SignInPageFragment(Dispatchers.IO))
    }

    @Deprecated(
        "Deprecated in Java",
        ReplaceWith("popFeature()", "project.rickandmorty.utils.extensions.popFeature")
    )
    override fun onBackPressed() {
        popFeature()
    }

    private fun replace(
        fragment: Fragment,
        tag: String = fragment::class.java.name
    ) {
        val fragmentManager = this.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction
            .replace(R.id.container, fragment, tag)
            .addToBackStack(tag)
            .commit()
    }
}
