package project.rickandmorty.menupage.ui

import android.view.View
import androidx.core.content.ContextCompat
import com.google.firebase.auth.FirebaseAuth
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineDispatcher
import project.rickandmorty.R
import project.rickandmorty.characterspage.di.CharactersPageModule.Companion.IoDispatcher
import project.rickandmorty.characterspage.ui.CharactersPageFragment
import project.rickandmorty.common.mvvm.BaseFragment
import project.rickandmorty.databinding.FragmentMenuPageBinding
import project.rickandmorty.locationspage.ui.LocationsPageFragment
import project.rickandmorty.seriespage.ui.SeriesPageFragment
import project.rickandmorty.signinpage.ui.SignInPageFragment
import project.rickandmorty.utils.extensions.replaceScreen
import project.rickandmorty.utils.viewbinding.viewBinding

@AndroidEntryPoint
class MenuPageFragment(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : BaseFragment(R.layout.fragment_menu_page) {

    private val binding: FragmentMenuPageBinding by viewBinding()
    override fun bind() {
        requireActivity().window.statusBarColor =
            ContextCompat.getColor(requireContext(), R.color.layout_background)

        with(binding) {
            buttonCharacters.setOnClickListener {
                replaceScreen(
                    CharactersPageFragment(),
                    enter = R.anim.nav_enter_nav
                )
            }
            buttonSeries.setOnClickListener {
                replaceScreen(SeriesPageFragment())
            }
            buttonLocations.setOnClickListener {
                replaceScreen(LocationsPageFragment())
            }
            textViewLogout.setOnClickListener {
                FirebaseAuth.getInstance().signOut()
                replaceScreen(SignInPageFragment(ioDispatcher), clearBackStack = true)
            }
        }
    }

    override fun initViews(view: View) {
    }

}
